[![pipeline status](https://git.coop/aptivate/ansible-roles/selinux/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/selinux/commits/master)

# selinux

A role to configure SELinux.

> We currently have no idea what SELinux does.
> So we're disabling it.
> GNU/Linux <3.

# Requirements

None.

# Role Variables

## Mandatory

* `selinux_disable`: Whether to disable SELinux or not.
  * Defaults to `true`.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: selinx
       selinux_disable: true
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

* https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

* https://aptivate.org/
* https://git.coop/aptivate
