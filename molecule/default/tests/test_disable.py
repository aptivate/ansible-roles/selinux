def test_selinux_disabled(host):
    ssh_config = host.file('/etc/selinux/config')
    with host.sudo():
        assert ssh_config.contains('SELINUX=disable')
